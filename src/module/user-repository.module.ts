import { Module } from '@nestjs/common';
import { DbModule } from '@neb-sports/mysamay-db-provider';

// Local Imports
import { PasswordRepository } from './../repositories/password-repository/password-repository';
import { UsersRepository } from '../repositories/users-repository/users.repository';

@Module({
    imports: [DbModule],
    providers: [UsersRepository, PasswordRepository],
    exports: [UsersRepository, PasswordRepository],
})
export class UserRepositoryModule {}
