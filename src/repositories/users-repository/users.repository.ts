import { Db, Collection, FilterQuery, SortOptionObject, SchemaMember, UpdateQuery } from 'mongodb';
import { UserEntity } from '@neb-sports/mysamay-users-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersRepository {
    db: Db;
    collection: Collection<UserEntity>;
    view: Collection<any>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(UserEntity.collectionName);
            this.view = this.db.collection("Users_UserSearchView");
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createIndexes() {
        await this.getCollection();
        this.collection.createIndexes([
            { key: { email: 1 }, name: "email_1", unique: true },
            { key: { mobile: 1 }, name: "mobile_1", unique: true },
            { key: { "address.city": 1 }, name: "city_1" },
            { key: { displayName: 1 }, name: "city_1" },
            { key: { gender: 1 }, name: "gender_1" },
            { key: { bloodGroup: 1 }, name: "bloodGroup_1" },
            { key: { createdTime: -1 }, name: "createdTime_1" },
            { key: { updatedTime: -1 }, name: "updatedTime_1" },
        ]);
    }

    async createUser(userEntity: UserEntity): Promise<UserEntity> {
        try {
            await this.getCollection();
            let savedUser = await this.collection.insertOne(userEntity);
            if (savedUser.insertedCount == 1) {
                return savedUser.ops[0];
            }
            return null;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async updateUser(userEntity: UserEntity): Promise<UserEntity> {
        try {
            await this.getCollection();
            let savedUser = await this.collection.findOneAndReplace(
                { _id: userEntity._id },
                userEntity,
                { returnOriginal: false },
            );
            if (savedUser.ok == 1) {
                return savedUser.value;
            }
            return null;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getUserById(id: string): Promise<UserEntity> {
        try {
            await this.getCollection();
            return await this.collection.findOne({ _id: id });
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getUserWithAnyQuery(
        query: FilterQuery<UserEntity>,
    ): Promise<UserEntity> {
        await this.getCollection();
        return await this.collection.findOne(query)
    }

    async countUserView(query: FilterQuery<any>) {
        await this.getCollection();
        let result = await this.view.countDocuments(query);
        return result;
    }

    async getUserView(query: FilterQuery<any>) {
        await this.getCollection();
        let result = await this.view.find(query).toArray();
        return result;
    }

    async searchUser(searchString: string): Promise<UserEntity[]> {
        try {
            await this.getCollection();
            let result = await this.collection
                .find({
                    $or: [
                        { email: '/.*' + searchString + '.*/' },
                        { given_name: '/.*' + searchString + '.*/' },
                        { family_name: '/.*' + searchString + '.*/' },
                    ],
                })
                .toArray();
            return result;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async countWithAnyQuery(query: FilterQuery<UserEntity>): Promise<number> {
        try {
            await this.getCollection();
            return await this.collection.countDocuments(query);
        } catch (error) {
            Promise.reject(error);
        }
    }

    async findWithAnyQuery(
        query: FilterQuery<UserEntity>,
    ): Promise<UserEntity[]> {
        try {
            await this.getCollection();
            return await this.collection.find(query).toArray();
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getAllUsers(projection: SchemaMember<UserEntity, any>): Promise<UserEntity[]> {
        await this.getCollection();
        return await this.collection
            .find({}, { projection: projection })
            .toArray();
    }

    async findWithAnyQueryPaginated(
        query: FilterQuery<UserEntity>,
        skip: number,
        limit: number,
        sort: SortOptionObject<UserEntity>,

    ): Promise<UserEntity[]> {
        try {
            await this.getCollection();
            return await this.collection
                .find(query)
                .collation({ locale: 'en' })
                .skip(skip)
                .limit(limit)
                .sort(sort)
                .toArray();
        } catch (error) {
            Promise.reject(error);
        }
    }

    async searchEventUserUsingJoin(userName: string, eventId: string, clubId: string, corporateId: string) {
        await this.getCollection();
        let query: FilterQuery<UserEntity> = {
            displayName: { $regex: ".*" + userName + ".*", $options: "i" }
        }
        if (clubId) query["cllub.clubId"] = clubId;
        if (corporateId) query["corporate.corporateId"] = corporateId;
        let result = await this.collection.aggregate([
            { $match: query },
            {
                $lookup: {
                    from: "Events_EventUserMap",
                    localField: "_id",
                    foreignField: "userId",
                    as: "events"
                }
            },
            { $match: { "events.eventId": eventId } },
            { $project: { temp: { $toLower: "$displayName" }, displayName: "$displayName" } },
            { $sort: { temp: 1 } }
        ]).skip(0).limit(5).toArray();
        if (result && result.length > 0) {
            return result;
        }
        return undefined;
    }

    async countUsersForAnEvent(eventId: string, clubId?: string, corporateId?: string, city?: string): Promise<number> {
        await this.getCollection();
        let query: FilterQuery<UserEntity> = {
        }
        if (clubId) query["club.clubId"] = clubId;
        if (corporateId) query["corporate.corporateId"] = corporateId;
        if (city) query["address.city"] = city;
        let result = await this.collection.aggregate<any>([
            { $match: query },
            {
                $lookup: {
                    from: "Events_EventUserMap",
                    localField: "_id",
                    foreignField: "userId",
                    as: "events"
                }
            },
            { $match: { "events.eventId": eventId } },
            { $count: "total" }
        ]).toArray();
        if (result && result.length > 0) {
            return result[0].total;
        }
        return undefined;
    }

    async getUsersAgeWiseStatistics<T>(query: FilterQuery<UserEntity>): Promise<T[]> {
        try {
            await this.getCollection();
            let result = await this.collection.aggregate<T>([
                { $match: query },
                {
                    "$group": {
                        "_id": {
                            "$concat": [
                                { "$cond": [{ "$lt": ["$age", 0] }, "Unknown", ""] },
                                { "$cond": [{ "$and": [{ "$gte": ["$age", 0] }, { "$lt": ["$age", 18] }] }, "Below 18", ""] },
                                { "$cond": [{ "$and": [{ "$gte": ["$age", 18] }, { "$lt": ["$age", 25] }] }, "18 to 25", ""] },
                                { "$cond": [{ "$and": [{ "$gte": ["$age", 26] }, { "$lt": ["$age", 35] }] }, "26 to 35", ""] },
                                { "$cond": [{ "$and": [{ "$gte": ["$age", 36] }, { "$lt": ["$age", 45] }] }, "36 to 45", ""] },
                                { "$cond": [{ "$and": [{ "$gte": ["$age", 46] }, { "$lt": ["$age", 55] }] }, "46 to 55", ""] },
                                { "$cond": [{ "$and": [{ "$gte": ["$age", 56] }, { "$lt": ["$age", 65] }] }, "56 to 65", ""] },
                                { "$cond": [{ "$gte": ["$age", 66] }, "Above 65", ""] }
                            ]
                        },
                        "count": { "$sum": 1 }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        ageGroup: "$_id",
                        count: "$count"
                    }
                }
            ]).toArray();
            if (result) return result
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getUsersCountryWiseStatistics<T>(query: FilterQuery<UserEntity>): Promise<T[]> {
        try {
            await this.getCollection();
            let result = await this.collection.aggregate<T>([
                { $match: query },
                { $group: { _id: "$address.country", count: { $sum: 1 } } },
                {
                    $project: {
                        _id: 0,
                        country: "$_id",
                        count: "$count"
                    }
                }
            ]).toArray();
            if (result) return result
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async deleteByAnyQuery(query: FilterQuery<UserEntity>) {
        await this.getCollection();
        await this.collection.deleteMany(query);
    }

    async getDistictFields(field: string) {
        await this.getCollection();
        let result = await this.collection.distinct(field);
        result = result.filter(r => {
            if (r && r != "") return true;
            return false;
        })
    }

    async groupByClub(query: FilterQuery<UserEntity>) {
        await this.getCollection();
        let result = await this.collection.aggregate([
            { $match: query },
            { $group: { _id: "$club.clubId" } }
        ]).toArray();
        if (result && result.length > 0) {
            let data = result.map(r => r._id);
            return data;
        }
        return null;
    }

    async groupByCorporate(query: FilterQuery<UserEntity>) {
        await this.getCollection();
        let result = await this.collection.aggregate([
            { $match: query },
            { $group: { _id: "$corporate.corporateId" } }
        ]).toArray();
        if (result && result.length > 0) {
            let data = result.map(r => r._id);
            return data;
        }
        return null;
    }

    async groupByCity(query: FilterQuery<UserEntity>) {
        await this.getCollection();
        let result = await this.collection.aggregate([
            { $match: query },
            { $group: { _id: "$address.city" } }
        ]).toArray();
        if (result && result.length > 0) {
            let data = result.map(r => r._id);
            return data;
        }
        return null;
    }




    async searchGroupUsers(q: string, query: FilterQuery<UserEntity>) {
        await this.getCollection();
        let result = await this.collection.find({
            $and: [
                query,
                { displayName: { $regex: ".*" + q + ".*", $options: "i" } }
            ]
        })
        .project({displayName: 1, email: 1, mobile: 1, gender: 1})
        .sort({ displayName: 1 })
        .skip(0)
        .limit(100)
        .toArray();
        return result;
    }

    async updateMany(filter: FilterQuery<UserEntity>, update: UpdateQuery<UserEntity | Partial<UserEntity>>) {
        await this.getCollection();
        await this.collection.updateMany(filter, update)
    }

}
