import { UserEntity } from '@neb-sports/mysamay-users-model';
import { UsersRepository } from './users.repository';
import { Test, TestingModule } from '@nestjs/testing';
import { DbModule } from '@neb-sports/mysamay-db-provider';

describe('UsersRepository', () => {
    let repo: UsersRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [UsersRepository],
        }).compile();

        repo = module.get<UsersRepository>(UsersRepository);
    });

    it('should be defined', async () => {
        expect(repo).toBeDefined();
    });

    it('should create user', async () => {
        let user: Partial<UserEntity> = {
            email: 'abc@gmail.com',
        };
        let userEntity = new UserEntity(user);
        let result = await repo.createUser(userEntity);
        expect(result._id).toBeDefined();
        expect(result.email).toBe('abc@gmail.com');
        await repo.disconnect();
    });

    it('should update user', async () => {
        let user: Partial<UserEntity> = {
            email: 'abc@gmail.com',
        };
        let userEntity = new UserEntity(user);
        let result = await repo.createUser(userEntity);
        result.age = 20;
        result = await repo.updateUser(result);
        expect(result.age).toBe(20);
        await repo.disconnect();
    });

    it('should get user by id', async () => {
        let user: Partial<UserEntity> = {
            email: 'abc@gmail.com',
        };
        let userEntity = new UserEntity(user);
        let result = await repo.createUser(userEntity);
        result = await repo.getUserById(result._id);
        expect(result).toBeDefined();
        await repo.disconnect();
    });

    it('should search user', async () => {
        let user: Partial<UserEntity> = {
            email: 'abc@gmail.com',
        };
        let userEntity = new UserEntity(user);
        await repo.createUser(userEntity);
        let res = await repo.searchUser('abc@gmail');
        expect(res).toBeDefined();
        await repo.disconnect();
    });

    it('should count with any query', async () => {
        let res = await repo.countWithAnyQuery({});
        expect(res).toBeDefined();
        await repo.disconnect();
    });
});
