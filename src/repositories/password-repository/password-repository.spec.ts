import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';
import { PasswordRepository } from './password-repository';

describe('PasswordRepository', () => {
    let provider: PasswordRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [PasswordRepository],
        }).compile();

        provider = module.get<PasswordRepository>(PasswordRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });
});
