import { FilterQuery } from 'mongodb';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { PasswordEntity } from '@neb-sports/mysamay-users-model';
import { Collection, Db } from 'mongodb';
import { Injectable } from '@nestjs/common';

@Injectable()
export class PasswordRepository {
    db: Db;
    collection: Collection<PasswordEntity>;

    constructor(private dbProvider: MongoProvider) {}

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(PasswordEntity.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createIndexes() {
        await this.getCollection();
        this.collection.createIndexes([
            {key: {userId: 1}, name: "userId_1", unique: true}
        ]);
    }

    async createPassword(
        passwordEntity: PasswordEntity,
    ): Promise<PasswordEntity> {
        try {
            await this.getCollection();
            let result = await this.collection.insertOne(passwordEntity);
            if (result.insertedCount === 1) return result.ops[0];
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async updatePassword(
        passwordEntity: PasswordEntity,
    ): Promise<PasswordEntity> {
        try {
            await this.getCollection();
            let result = await this.collection.findOneAndReplace(
                { _id: passwordEntity._id },
                passwordEntity,
                { returnOriginal: false },
            );
            if (result.ok === 1) return result.value;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getPasswordByAnyQuery(
        query: FilterQuery<PasswordEntity>,
    ): Promise<PasswordEntity> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query);
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async deleteByAnyQuery(query: FilterQuery<PasswordEntity>) {
        await this.getCollection();
        await this.collection.deleteMany(query);
    }
}
